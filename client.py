import socket

IP = "255.255.255.255"
PORT = 8000
MAX_SIZE = 1024

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)

# Enable broadcasting mode
sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)

sock.bind((IP, PORT))

while True:
    data, addr = sock.recvfrom(MAX_SIZE)
    print("Got ", data, " from ", addr)
