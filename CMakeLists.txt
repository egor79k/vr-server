# Some changes
cmake_minimum_required(VERSION 2.8)

project(server)

add_library(lib_server server.cpp)

add_executable(server main.cpp)

target_link_libraries(server lib_server)
